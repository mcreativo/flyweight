<?php


class FlyweightFactory
{
    /**
     * @var Flyweight[]
     */
    protected $flyweights;

    /**
     * @param string $intrinsic
     * @return Service
     */
    public function getService($intrinsic)
    {
        if (!isset($this->flyweights[$intrinsic])) {
            $flyweight = new Service($intrinsic);
            $this->flyweights[$intrinsic] = $flyweight;
        }
        return $this->flyweights[$intrinsic];
    }
} 