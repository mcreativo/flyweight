<?php


interface Flyweight
{
    /**
     * @param string $extrinsic
     */
    public function setExtrinsic($extrinsic);
} 