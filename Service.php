<?php


class Service implements Flyweight
{
    /**
     * @var string
     */
    protected $intrinsic;

    /**
     * @var string
     */
    protected $extrinsic;


    function __construct($intrinsic)
    {
        $this->intrinsic = $intrinsic;
        echo sprintf('creating service with parameters %s %s ', $this->intrinsic, $this->extrinsic) . PHP_EOL;
    }


    public function doSomething()
    {
        echo sprintf('doing something with parameters %s %s ', $this->intrinsic, $this->extrinsic) . PHP_EOL;
    }

    /**
     * @param string $extrinsic
     */
    public function setExtrinsic($extrinsic)
    {
        $this->extrinsic = $extrinsic;
    }
} 